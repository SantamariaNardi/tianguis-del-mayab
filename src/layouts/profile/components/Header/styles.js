/**
=========================================================
* Soft UI Dashboard React - v2.0.0
=========================================================

* Product Page: https://www.creative-tim.com/product/soft-ui-dashboard-pro-material-ui
* Copyright 2021 Creative Tim (https://www.creative-tim.com)

Coded by www.creative-tim.com

 =========================================================

* The above copyright notice and this permission notice shall be included in all copies or substantial portions of the Software.
*/

// @mui material components
import { makeStyles } from "@mui/styles";

// Images
import curved0 from "assets/images/curved-images/cabecera2.jpg";

export default makeStyles(({ functions, boxShadows }) => {
  const { pxToRem } = functions;
  const { navbarBoxShadow } = boxShadows;

  return {
    profileHeader_profile: {
      backgroundColor: "#328914",
      boxShadow: navbarBoxShadow,
      padding: pxToRem(16),
      position: "relative",
    },
    profileHeader_background: {
      backgroundImage: `url(${curved0})`,
      backgroundSize: "cover",
      backgroundPosition: "50%",
      display: "flex",
      alignItems: "center",
      position: "relative",
      overflow: "hidden",
      minHeight: pxToRem(200),
    },
    sidenav_logo: {
      width: pxToRem(55),
    },
  };
});
