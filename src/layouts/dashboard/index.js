/**
=========================================================
* Soft UI Dashboard React - v2.0.0
=========================================================

* Product Page: https://www.creative-tim.com/product/soft-ui-dashboard-material-ui
* Copyright 2021 Creative Tim (https://www.creative-tim.com)

Coded by www.creative-tim.com

 =========================================================

* The above copyright notice and this permission notice shall be included in all copies or substantial portions of the Software.
*/

// @mui material components
import Grid from "@mui/material/Grid";

// Soft UI Dashboard React components
import SuiBox from "components/SuiBox";
import { useLocation } from "react-router-dom";

// Soft UI Dashboard React example components
import DashboardLayout from "examples/LayoutContainers/DashboardLayout";
import DashboardNavbar from "examples/Navbars/DashboardNavbar";
import Footer from "examples/Footer";
import ReportsBarChart from "examples/Charts/BarCharts/ReportsBarChart";
import Breadcrumbs from "examples/Breadcrumbs";

// Soft UI Dashboard React base styles
// Data
import reportsBarChartData from "layouts/dashboard/data/reportsBarChartData";
import styles from "layouts/profile/components/Header/styles";
import Producto from "./producto"

function Overview() {
  const { chart, items } = reportsBarChartData;
  const classes = styles();
  const route = useLocation().pathname.split("/").slice(1);
  return (
    <DashboardLayout>
      <DashboardNavbar light />
      <SuiBox customClass={classes.profileHeader_background} />
      <SuiBox py={1}>
        <SuiBox mt={1} mb={1} ml={3} mr={3}>
          <SuiBox customClass={classes.navbar_row} color="inherit" ml={1}>
            <Breadcrumbs icon="home" title={route[route.length - 1]} route={route} />
          </SuiBox>
          <Producto/>
        </SuiBox>
      </SuiBox>
      <Footer />
    </DashboardLayout>
  );
}

export default Overview;
