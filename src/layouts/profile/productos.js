import React from "react";
import { Link } from "react-router-dom";
import Grid from "@mui/material/Grid";
import ReportsBarChart from "examples/Charts/BarCharts/ReportsBarChart";
import reportsBarChartData from "layouts/dashboard/data/reportsBarChartData";
import wavesWhite from "assets/images/shapes/waves-white.svg";
import rocketWhite from "assets/images/illustrations/rocket-white.png";
// porp-types is a library for typechecking of props
import PropTypes from "prop-types";
// react-chartjs-2 components
// @mui material components
import Card from "@mui/material/Card";

// Soft UI Dashboard React components
import SuiBox from "components/SuiBox";
import SuiTypography from "components/SuiTypography";
import { get } from "../../api";

export default class ListaProductos extends React.Component {
  constructor(props) {
    super(props);
    this.state = { productos: [], categories: {} };
  }

  componentDidMount() {
    const apiCall = async () => {
      const { data: { data, categories } } = await get('/getProducts');
      console.log('----------------');
      console.log(data, categories);
      const cats = categories.reduce((acc, it) => (
        { ...acc, [`id-${it.id}`]: it }
        ), {});
      this.setState({ productos: [...data], categories: { ...cats } });
    }
 
    apiCall();
  }

  render() {
    const { productos, categories } = this.state;
    console.log(categories)
    const { chart, items } = reportsBarChartData;

    // const productos = this.isUpdatedByChange ? propValue : stateValue;
    // it will be ok when I didn't use Destructuring
    // const value = this.isUpdatedByChange ? this.props.value: this.state.value;
    return <Grid container spacing={2} mt={1}>
      {productos.map(item => (
        <Grid item xs={12} md={4} xl={2}>
          <Link to={`/producto/${item.id}`}>
          <Card>
            <SuiBox padding="1rem">
              <Grid item xs={12} lg={12} className="relative">
                <SuiTypography variant="h6" textTransform="capitalize">
                  {item.name}
                </SuiTypography>
                <SuiBox
                  display="grid"
                  justifyContent="center"
                  alignItems="center"
                  /* backgroundColor="info" */
                  borderRadius="lg"
                  backgroundGradient
                >
                  <SuiBox component="img" src={item.img} alt="rocket" width="200px" p={3} />
                </SuiBox>
              </Grid>
              <SuiBox px={1}>
                <SuiBox mb={2}>
                  <SuiTypography variant="button" textColor="text" fontWeight="bold">
                    ${item.price}<br/>
                  </SuiTypography>
                  <SuiTypography variant="button" textColor="text" fontWeight="regular">
                    {item.description}
                  </SuiTypography>
                  <br/>
                  <SuiTypography variant="button" textColor="text" fontWeight="regular">
                    Categoria: {categories[`id-${item.category_id}`].name}
                  </SuiTypography>
                </SuiBox>
              </SuiBox>
            </SuiBox>
          </Card>
          </Link>
          {/* <ReportsBarChart
                title={item.name}
                description={
                  <>
                    <strong>+3</strong>
                    <p>
                    {item.description}
                    </p>
                  </>
                }
                chart={chart}
                items={items}
              /> */}
        </Grid>
      ))}
      </Grid>;
    /* render() {
        return (
          <div>
            { this.state.productos.map(producto =>{producto.name})}
          </div>
        )
      } */
  }
}
  
