import React from "react";
import { withRouter } from 'react-router-dom';

import Grid from "@mui/material/Grid";
import Card from "@mui/material/Card";
import Icon from "@mui/material/Icon";

// Soft UI Dashboard React components
import SuiBox from "components/SuiBox";
import SuiTypography from "components/SuiTypography";

// Custom styles for the BuildByDevelopers
import styles from "layouts/rtl/components/BuildByDevelopers/styles";

// Images
import wavesWhite from "assets/images/shapes/waves-white.svg";
import rocketWhite from "assets/images/illustrations/rocket-white.png";
import { get } from "../../api";

class Productor extends React.Component {
  constructor(props) {
    super(props);
    this.state = { productor:{} };
  }

  componentDidMount() {
    const { match: { params: { id: producerId } } } = this.props;
    const apiCall = async () => {
      const { data: { data } } = await get(`/getProducer/${producerId}`);
      console.log('----------------');
      console.log(data);
      this.setState( { productor: data } );
    }

    apiCall();
  }

  render() {
      const { productor } = this.state;
      return (
      <Card>
      <SuiBox p={2}>
        <Grid container spacing={3}>
          <Grid item xs={12} lg={5} className="mr-auto relative">
            <SuiBox
              display="grid"
              justifyContent="center"
              alignItems="center"
              borderRadius="lg"
              backgroundGradient
            >
              <SuiBox component="img" src={productor.img} alt={productor.img} width="200px" pt={3} />
            </SuiBox>
          </Grid>
          <Grid item xs={12} lg={6}>
            <SuiBox display="flex" flexDirection="column" height="100%">
              <SuiTypography variant="h5" fontWeight="bold" gutterBottom>
              {productor.name}
              </SuiTypography>
              <SuiBox mb={6}>
                <SuiTypography variant="body2" textColor="text">
                  {productor.history}
                </SuiTypography>
              </SuiBox>
              <SuiTypography
                component="a"
                href={`/productosporproductor/${productor.id}`}
                variant="button"
                textColor="text"
                fontWeight="bold"
                
              >
                Ver productos de {productor.name}
                <Icon className="font-bold">arrow_backward</Icon>
              </SuiTypography>
            </SuiBox>
          </Grid>
        </Grid>
      </SuiBox>
      </Card>
      )
  } 
}
export default withRouter(Productor);
