import React from "react";
import axios from "axios";

export default class ListaProductos extends React.Component {
  state = {
    productos: []
  }

  componentDidMount() {
    axios.get(`http://localhost:8000/api/getProducts`)
      .then(res => {
        const productos = res.data;
        this.setState({ productos });
      })
  }

  render() {
    return (
      <div>
        { this.state.productos.map(producto =>{producto.name})}
      </div>
    )
  }
}