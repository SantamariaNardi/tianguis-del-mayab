import React from "react";
import { Link } from "react-router-dom";

import axios from "axios";
import FacebookIcon from "@mui/icons-material/Facebook";
import TwitterIcon from "@mui/icons-material/Twitter";
import Grid from "@mui/material/Grid";
import InstagramIcon from "@mui/icons-material/Instagram";
import ProfileInfoCard from "examples/Cards/InfoCards/ProfileInfoCard";
import { get } from "../../api";

export default class ListaProductores extends React.Component {
  constructor(props) {
    super(props);
    this.state = { productores: [] };
  }

  componentDidMount() {
    const apiCall = async () => {
      const { data: { data } } = await get('/getProducers');
      console.log('----------------');
      console.log(data);
      this.setState({ productores: [...data] });
    }

    apiCall();
  }

  render() {
    const { productores } = this.state;
    // const productos = this.isUpdatedByChange ? propValue : stateValue;
    // it will be ok when I didn't use Destructuring
    // const value = this.isUpdatedByChange ? this.props.value: this.state.value;
    return <Grid container spacing={2} mt={1}>
      {productores.map(item => (
      
        <Grid item xs={12} md={6} xl={3}>
          <Link to={`/productor/${item.id}`}>
          <ProfileInfoCard
            title={item.name}
           /*  description={item.city} */
            info={{
              Telefono: item.phone, 
              Ubicación: item.city
            }}
            social={[
              {
                link: "https://www.facebook.com/CreativeTim/",
                icon: <FacebookIcon />,
                color: "facebook",
              },
              {
                link: "https://twitter.com/creativetim",
                icon: <TwitterIcon />,
                color: "twitter",
              },
              {
                link: "https://www.instagram.com/creativetimofficial/",
                icon: <InstagramIcon />,
                color: "instagram",
              },
            ]}
            imagenproducto={item.img}
            action={{ route: "", tooltip: "Edit Profile" }}
          />
        </Link>
        </Grid>
      ))}
      </Grid>;
    /* render() {
        return (
          <div>
            { this.state.productos.map(producto =>{producto.name})}
          </div>
        )
      } */
  }
}
  
