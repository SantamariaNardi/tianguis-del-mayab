/**
=========================================================
* Soft UI Dashboard React - v2.0.0
=========================================================

* Product Page: https://www.creative-tim.com/product/soft-ui-dashboard-pro-material-ui
* Copyright 2021 Creative Tim (https://www.creative-tim.com)

Coded by www.creative-tim.com

 =========================================================

* The above copyright notice and this permission notice shall be included in all copies or substantial portions of the Software.
*/

// @mui material components
import Grid from "@mui/material/Grid";

// @mui icons
import FacebookIcon from "@mui/icons-material/Facebook";
import TwitterIcon from "@mui/icons-material/Twitter";
import InstagramIcon from "@mui/icons-material/Instagram";

// Soft UI Dashboard React components
import SuiBox from "components/SuiBox";

// Soft UI Dashboard React example components
import DashboardLayout from "examples/LayoutContainers/DashboardLayout";
import Footer from "examples/Footer";
import ProfileInfoCard from "examples/Cards/InfoCards/ProfileInfoCard";
import DashboardNavbar from "examples/Navbars/DashboardNavbar";
import Breadcrumbs from "examples/Breadcrumbs";

// Overview page components
import { useLocation } from "react-router-dom";
// Data
// Images
import styles from "layouts/profile/components/Header/styles";
import ListaProductores from "./productores";

function Overview() {
  const route = useLocation().pathname.split("/").slice(1);
  const classes = styles();
  return (
    <DashboardLayout>
      <DashboardNavbar light />
      <SuiBox customClass={classes.profileHeader_background} />
      <SuiBox mt={3} mb={1} ml={3} mr={3}>
        <SuiBox customClass={classes.navbar_row} color="inherit" ml={1}>
          <Breadcrumbs icon="home" title={route[route.length - 1]} route={route} />
        </SuiBox>
        <ListaProductores/>
      </SuiBox>
      <Footer />
    </DashboardLayout>
  );
}

export default Overview;
