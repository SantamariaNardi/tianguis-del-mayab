import React from "react";
import { withRouter } from 'react-router-dom';
import Grid from "@mui/material/Grid";
import reportsBarChartData from "layouts/dashboard/data/reportsBarChartData";
import ReportsBarChart from "examples/Charts/BarCharts/ReportsBarChart";
import Card from "@mui/material/Card";

// Soft UI Dashboard React components
import SuiBox from "components/SuiBox";
import SuiTypography from "components/SuiTypography";
import { get } from "../../api";

class Producto extends React.Component {
  constructor(props) {
    super(props);
    this.state = { producto: {}, productor:{} };
  }

  componentDidMount() {
    const { match: { params: { id: productId } } } = this.props;
    const apiProduct = async () => {
      const { data: { data } } = await get(`/getProduct/${productId}`);
      // console.log('----------------');
      // console.log(data);
      this.setState( { producto: data } );
      return data;
    }
    const apiProducer = async (producerId) => {
      const { data: { data } } = await get(`/getProducer/${producerId}`);
      // console.log('----------------');
      // console.log(data);
      this.setState( { productor: data } );
    }
    const getData = async () => {
      const prod = await apiProduct();
      apiProducer(prod.producer_id);
    }

    getData();
  }

  render() {
    const { chart, items } = reportsBarChartData;
    const { producto } = this.state;
    const { productor } = this.state;
      return (
        <Grid container spacing={4} mt={0} xs={13}>
            <Grid item lg={4} p={1}>
              <Card>
                <SuiBox padding="1rem">
                  <Grid item xs={12} lg={12} className="relative">
                    <SuiTypography variant="h6" textTransform="capitalize">
                      {productor.name}
                    </SuiTypography>
                    <SuiBox
                      display="grid"
                      justifyContent="center"
                      alignItems="center"
                      /* backgroundColor="info" */
                      borderRadius="lg"
                      backgroundGradient
                    >
                      <SuiBox component="img" src={productor.img} alt="rocket" width="150px" pt={3} />
                    </SuiBox>
                  </Grid>
                  <SuiBox px={1}>
                    <SuiBox mb={2}>
                    
                      <SuiTypography variant="button" textColor="text" fontWeight="regular">
                      {productor.history}
                      </SuiTypography>
                    </SuiBox>
                  </SuiBox>
                </SuiBox>
              </Card>
            </Grid>
            <Grid item lg={8}>
            <Card>
                <SuiBox padding="1rem">
                  <Grid item xs={12} lg={12} className="relative">
                    <SuiTypography variant="h6" textTransform="capitalize">
                      {producto.name}
                    </SuiTypography>
                    <SuiBox
                      display="grid"
                      justifyContent="center"
                      alignItems="center"
                      /* backgroundColor="info" */
                      borderRadius="lg"
                      backgroundGradient
                    >
                      <SuiBox component="img" src={producto.img} alt="rocket" width="250px" pt={3} />
                    </SuiBox>
                  </Grid>
                  <SuiBox px={1}>
                    <SuiBox mb={2}>
                    
                      <SuiTypography variant="button" textColor="text" fontWeight="regular">
                      {producto.description}
                      </SuiTypography>
                    </SuiBox>
                  </SuiBox>
                </SuiBox>
              </Card>
            </Grid>
          </Grid>
      )
  } 
}
  
export default withRouter(Producto);
