/**
=========================================================
* Soft UI Dashboard React - v2.0.0
=========================================================

* Product Page: https://www.creative-tim.com/product/soft-ui-dashboard-material-ui
* Copyright 2021 Creative Tim (https://www.creative-tim.com)

Coded by www.creative-tim.com

 =========================================================

* The above copyright notice and this permission notice shall be included in all copies or substantial portions of the Software.
*/

// @mui material components
import Grid from "@mui/material/Grid";

// Soft UI Dashboard React components
import SuiBox from "components/SuiBox";

// Soft UI Dashboard React example components
import DashboardLayout from "examples/LayoutContainers/DashboardLayout";
import DashboardNavbar from "examples/Navbars/DashboardNavbar";
import Footer from "examples/Footer";
import BuildByDevelopers from "layouts/rtl/components/BuildByDevelopers";
import Breadcrumbs from "examples/Breadcrumbs";
import styles from "layouts/profile/components/Header/styles";
import { useLocation } from "react-router-dom";
import Productor from "./productor";
// Soft UI Dashboard React base styles
// Data

function Dashboard() {
  const classes = styles();
  const route = useLocation().pathname.split("/").slice(1);
  return (
    <DashboardLayout>
      <DashboardNavbar light />
      <SuiBox customClass={classes.profileHeader_background} />
      <SuiBox py={3}>
        <SuiBox mt={1} mb={1} ml={3} mr={3}>
          <SuiBox customClass={classes.navbar_row} color="inherit" ml={1}>
            <Breadcrumbs icon="home" title={route[route.length - 1]} route={route} />
          </SuiBox>
          <Grid container spacing={2}>
            <Grid item xs={12} lg={4}>
              <Productor/>
              hola mundo
            </Grid>
            <Grid item xs={12} lg={8}>
              <BuildByDevelopers />
            </Grid>
          </Grid>
        </SuiBox>
      </SuiBox>
      <Footer />
    </DashboardLayout>
  );
}

export default Dashboard;
