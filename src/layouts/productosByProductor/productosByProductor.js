import React from "react";
import Card from "@mui/material/Card";
import { withRouter, Link } from 'react-router-dom';
import Grid from "@mui/material/Grid";
import reportsBarChartData from "layouts/dashboard/data/reportsBarChartData";
import SuiBox from "components/SuiBox";
import SuiTypography from "components/SuiTypography";
import { get } from "../../api";

class ListaProductosByProductor extends React.Component {
  constructor(props) {
    super(props);
    this.state = { productos: [] };
  }

  componentDidMount() {
    const { match: { params: { id: producerId } } } = this.props;
    const apiCall = async () => {
      const { data: { data } } = await get(`/getProductsByProducer/${producerId}`);
      console.log('----------------');
      console.log(data);
      this.setState({ productos: [...data] });
    }

    apiCall();
  }

  render() {
    const { productos } = this.state;
    const { chart, items } = reportsBarChartData;

    // const productos = this.isUpdatedByChange ? propValue : stateValue;
    // it will be ok when I didn't use Destructuring
    // const value = this.isUpdatedByChange ? this.props.value: this.state.value;
    return <Grid container spacing={2} mt={1}>
        {/* <h5 style={{paddingLeft: 8, paddingTop:15}}>MIELIPONA</h5> */}
      {productos.map(item => (
          <Grid item xs={12} md={4} xl={2}>
          <Link to={`/producto/${item.id}`}>
          <Card>
            <SuiBox padding="1rem">
              <Grid item xs={12} lg={12} className="relative">
                <SuiTypography variant="h6" textTransform="capitalize">
                  {item.name}
                </SuiTypography>
                <SuiBox
                  display="grid"
                  justifyContent="center"
                  alignItems="center"
                  /* backgroundColor="info" */
                  borderRadius="lg"
                  backgroundGradient
                >
                  <SuiBox component="img" src={item.img} alt="rocket" width="200px" p={3} />
                </SuiBox>
              </Grid>
              <SuiBox px={1}>
                <SuiBox mb={2}>
                  <SuiTypography variant="button" textColor="text" fontWeight="bold">
                    ${item.price}<br/>
                  </SuiTypography>
                  <SuiTypography variant="button" textColor="text" fontWeight="regular">
                    {item.description}
                  </SuiTypography>
                  <br/>
                  <SuiTypography variant="button" textColor="text" fontWeight="regular">
                    Categoria: {item.category_id}
                  </SuiTypography>
                </SuiBox>
              </SuiBox>
            </SuiBox>
          </Card>
          </Link>
          {/* <ReportsBarChart
                title={item.name}
                description={
                  <>
                    <strong>+3</strong>
                    <p>
                    {item.description}
                    </p>
                  </>
                }
                chart={chart}
                items={items}
              /> */}
        </Grid>
      ))}
      </Grid>;
    /* render() {
        return (
          <div>
            { this.state.productos.map(producto =>{producto.name})}
          </div>
        )
      } */
  }
}
export default withRouter(ListaProductosByProductor);
