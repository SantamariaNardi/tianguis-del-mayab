/**
=========================================================
* Soft UI Dashboard React - v2.0.0
=========================================================

* Product Page: https://www.creative-tim.com/product/soft-ui-dashboard-material-ui
* Copyright 2021 Creative Tim (https://www.creative-tim.com)

Coded by www.creative-tim.com

 =========================================================

* The above copyright notice and this permission notice shall be included in all copies or substantial portions of the Software.
*/

import wavesWhite from "assets/images/shapes/waves-white.svg";
import rocketWhite from "assets/images/illustrations/rocket-white.png";

// porp-types is a library for typechecking of props
import PropTypes from "prop-types";

// react-chartjs-2 components
// @mui material components
import Card from "@mui/material/Card";
import Grid from "@mui/material/Grid";

// Soft UI Dashboard React components
import SuiBox from "components/SuiBox";
import SuiTypography from "components/SuiTypography";

// Soft UI Dashboard React example components

// ReportsBarChart configurations

function ReportsBarChart({ title, description, imagen }) {
  /*  const renderItems = items.map(({ icon, label, progress }) => (
    <Grid item xs={6} sm={3} key={label}>
      <BarReportsChartItem
        color={color}
        icon={{ color: icon.color, component: icon.component }}
        label={label}
        progress={{ content: progress.content, percentage: progress.percentage }}
      />
    </Grid>
  )); */
  return (
    <Card>
      <SuiBox padding="1rem">
        <Grid item xs={12} lg={12} className="relative">
          <SuiTypography variant="h6" textTransform="capitalize">
            {title}
          </SuiTypography>
          <SuiBox
            display="grid"
            justifyContent="center"
            alignItems="center"
            /* backgroundColor="info" */
            borderRadius="lg"
            backgroundGradient
          >
            <SuiBox component="img" src={imagen} alt={imagen} width="250px" pt={3} />
          </SuiBox>
        </Grid>
        <SuiBox px={1}>
          <SuiBox mb={2}>
           
            <SuiTypography variant="button" textColor="text" fontWeight="regular">
              {description}
            </SuiTypography>
          </SuiBox>
        </SuiBox>
      </SuiBox>
    </Card>
  );
}

// Setting default values for the props of ReportsBarChart
ReportsBarChart.defaultProps = {
  description: "",
};

// Typechecking props for the ReportsBarChart
ReportsBarChart.propTypes = {
  /* color: PropTypes.oneOf(["primary", "secondary", "info", "success", "warning", "error", "dark"]), */
  title: PropTypes.string.isRequired,
  description: PropTypes.oneOfType([PropTypes.string, PropTypes.node]),
  /*   items: PropTypes.arrayOf(PropTypes.object), */
};

export default ReportsBarChart;
