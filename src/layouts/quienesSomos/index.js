// Soft UI Dashboard React components
import SuiBox from "components/SuiBox";
// Soft UI Dashboard React example components
import DashboardLayout from "examples/LayoutContainers/DashboardLayout";
import Footer from "examples/Footer";
import DashboardNavbar from "examples/Navbars/DashboardNavbar";
import SuiTypography from "components/SuiTypography";

import styles from "layouts/profile/components/Header/styles";
import { AlignHorizontalCenter } from "@mui/icons-material";


function Overview() {
  const classes = styles();
  return (
    <DashboardLayout>
      <DashboardNavbar light />
      <SuiBox customClass={classes.profileHeader_background} />
      <SuiBox mt={3} mb={1} ml={3} mr={3}>
        <SuiBox>
            <h2 style={{textAlign:'center', background: '#D6DBDF ', color:'black', fontWeight: 'normal'}}>¿Quienes Somos?</h2>
        </SuiBox>
        <SuiBox component="img" src="tianguisdelM.jpg" alt="rocket" width="300px" p={3} />
        <SuiTypography style={{color:'black'}} variant="button" textColor="text" fontWeight="regular">
            LoremIpsu
        </SuiTypography>
      </SuiBox>
      <SuiBox mt={3} mb={1} ml={50} mr={3}>
        <SuiTypography style={{color:'black'}} variant="button" textColor="text" fontWeight="regular">
            LoremIpsu
        </SuiTypography>
        <SuiBox component="img" src="productor1.jpg" alt="rocket" width="300px" p={3} />
      </SuiBox>
      <SuiBox mt={3} mb={1} ml={100} mr={3}>
        <SuiTypography style={{color:'black'}} variant="button" textColor="text" fontWeight="regular">
            LoremIpsu
        </SuiTypography>
        <SuiBox component="img" src="Bienvenidos.jpg" alt="rocket" width="300px" p={3} />
      </SuiBox>
      <Footer />
    </DashboardLayout>
  );
}

export default Overview;
