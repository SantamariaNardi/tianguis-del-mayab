// @mui material components
import Grid from "@mui/material/Grid";

// Soft UI Dashboard React components
import SuiBox from "components/SuiBox";

// Soft UI Dashboard React example components
import DashboardLayout from "examples/LayoutContainers/DashboardLayout";
import DashboardNavbar from "examples/Navbars/DashboardNavbar";
import Footer from "examples/Footer";
import BuildByDevelopers from "layouts/rtl/components/BuildByDevelopers";
import Breadcrumbs from "examples/Breadcrumbs";
import styles from "layouts/profile/components/Header/styles";
import { useLocation } from "react-router-dom";
import FacebookIcon from "@mui/icons-material/Facebook";
import TwitterIcon from "@mui/icons-material/Twitter";
import WhatsAppIcon from '@mui/icons-material/WhatsApp';
import InstagramIcon from "@mui/icons-material/Instagram";
import SocialInfoCard from "./Social-InfoCard";

import Productor from "./productor";
// Soft UI Dashboard React base styles
// Data

function Dashboard() {
  const classes = styles();
  const route = useLocation().pathname.split("/").slice(1);
  return (
    <DashboardLayout>
      <DashboardNavbar light />
      <SuiBox customClass={classes.profileHeader_background} />
      <SuiBox py={3}>
        <SuiBox mt={1} mb={1} ml={3} mr={3}>
          <SuiBox customClass={classes.navbar_row} color="inherit" ml={1}>
            <Breadcrumbs icon="home" title={route[route.length - 1]} route={route} />
          </SuiBox>
          <Grid container spacing={2}>
            <Grid item xs={12} lg={4}>
              
              <SocialInfoCard
              title="profile information"
              description="Hi, I’m Alec Thompson, Decisions: If you can’t decide, the answer is no. If two equally difficult paths, choose the one more painful in the short term (pain avoidance is creating an illusion of equality)."
              info={{
                fullName: "Alec M. Thompson",
                mobile: "(44) 123 1234 123",
                email: "alecthompson@mail.com",
                location: "USA",
              }}
              social={[
                {
                  link: "https://www.facebook.com",
                  icon: <FacebookIcon />,
                  color: "facebook",
                },
                {
                  link: "https://twitter.com",
                  icon: <WhatsAppIcon />,
                  color: "whatsapp",
                },
                {
                  link: "https://www.instagram.com",
                  icon: <InstagramIcon />,
                  color: "instagram",
                },
              ]}
              action={{ route: "", tooltip: "Edit Profile" }}
              />
            </Grid>
            <Grid item xs={12} lg={8}>
              <Productor/>
            </Grid>
          </Grid>
        </SuiBox>
      </SuiBox>
      <Footer />
    </DashboardLayout>
  );
}

export default Dashboard;
